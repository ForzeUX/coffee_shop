"use strict"

const burgerMenu = document.querySelector('#burger');
const overlay = document.querySelector('#nav')
const menuLinks = document.querySelectorAll('navigation__item')

burgerMenu.addEventListener('click', () => {
  burgerMenu.classList.toggle('header__burger_active');
  overlay.classList.toggle('navigation__active');
});

// Блок обратной связи

const btnSubmit = document.querySelector('#submit')
const blockSuccess = document.querySelector('#success')
const blockError = document.querySelector('#error')


const fieldName = document.querySelector('#name')
console.log(fieldName);
const fieldEmail = document.querySelector('#email')
console.log(fieldEmail);
const fieldMessage = document.querySelector('#message')
console.log(fieldMessage);


btnSubmit.addEventListener('click' , (event) =>{
  event.preventDefault();

  if (fieldName.value === '' || fieldEmail.value === '' || fieldMessage.value === '') {
    blockError.classList.add('form__fields-error_active');
  }

  else {
    blockSuccess.classList.add('wrapper-success_active');
    blockError.classList.remove('form__fields-error_active');
    setTimeout(() => {blockSuccess.classList.remove('wrapper-success_active') }, 3000);
    document.getElementById('myForm').reset()
  }
});


